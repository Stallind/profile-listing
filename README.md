# To run the project:
* Download/clone the repo
* Navigate to the project folder and do `npm install` followed by `npm start`

## About the project
This is a project where users is sorted/filtered and displayed.

Time spent for me to complete the site to this degree was me 5h.32m.

Because of the time limit I decided to bootstrap the project with CRA and design it using MDBReact.


### Design:
* Fancy animations 
* A modern design
* Responsive design

### Functionallity:
* Sort by name or office
* Filter by name
* Enable switch to a different view
* Hosted on free public url

### Testing:
* Works in chrome, firefox and edge




This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).