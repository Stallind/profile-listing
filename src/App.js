import React from 'react';
import './App.css';
import Home from './components/PageHome/Home';

function App() {
  return (
    <div className="App bg-light">
      <Home />
    </div>
  );
}

export default App;
