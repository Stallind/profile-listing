import React, { useEffect, useState } from 'react';
import './SearchField.css'

const SearchField = ({ users, setDisplayedUsers }) => {

  const [searchValue, setSearchValue] = useState('')

  useEffect(() => {
    const timeout = setTimeout(()=> {
      
      if (searchValue.length === 0){
        const userBankArray = [...users]
        const sortedByName = userBankArray.sort((a,b) => a.name.localeCompare(b.name))
        setDisplayedUsers(sortedByName);
      }

      if (searchValue.length !== 0) {
    let search = users.filter((x => x.name.toLowerCase().includes(searchValue.toLowerCase())))
    setDisplayedUsers(search);
      }
    }, 400);
    return () => clearTimeout(timeout);
    // eslint-disable-next-line
  },[searchValue, users])

  const handleSearch = (e) => {
    setSearchValue(e.target.value);
  };

  return (
      <div className="w-25 active-green notActive mt-5">
        <input
        className="form-control"
        type="text"
        placeholder="Search by name"
        value={searchValue}
        onChange={handleSearch}
        />
      </div>
  )
}

export default SearchField;