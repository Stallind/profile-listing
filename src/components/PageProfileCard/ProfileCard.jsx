import React from 'react'
import {
  MDBAnimation,
  MDBCard,
  MDBCardBody,
  MDBCol,
  MDBContainer,
  MDBIcon,
  MDBRow,
} from 'mdbreact';
import './ProfileCard.css'

const ProfileCard = ({ users, isLeetView }) => {

  return (
      <>
        {!isLeetView ? (
      <MDBCol size="11" sm="6" md="3" className="p-0 m-0">
        <MDBAnimation type="zoomIn" duration="1s" className="d-flex justify-content-center">
          <MDBCard className="mt-3" style={{ width: "90%" }}>
          <div className="text-center" style={{ minHeight: "199px" }}>
            <img
                alt={`${users.name}`}
                className="img-fluid d-inline"
                src={users.imagePortraitUrl}
            />
            </div>
            <MDBCardBody>
              <MDBContainer className="w-100 p-0">
                <MDBRow className="w-100 m-0">
                  <MDBCol size="7" className="p-0 m-0">
                    <p className="m-0 p-0 cardText" style={{ minHeight: "45px" }}>
                      {users.name}
                    </p>
                  </MDBCol>
                  <MDBCol size="5" className="m-0 p-0 text-right">
                    {users.linkedIn && (
                        <a className="blackLink" href={`https://linkedin.com/${users.linkedIn}`}>
                          <MDBIcon fab icon="linkedin" className="ml-2" />
                        </a>
                    )}
                    {users.gitHub && (
                        <a className="blackLink" href={`https://github.com/${users.gitHub}`}>
                          <MDBIcon fab icon="github-square" className="ml-2" />
                        </a>
                    )}
                    {users.twitter && (
                        <a className="blackLink" href={`https://twitter.com/${users.twitter}`}>
                          <MDBIcon fab icon="twitter" className="ml-2" />
                        </a>
                    )}
                    {users.stackOverflow && (
                        <a className="blackLink" href={`https://stackoverflow.com/users/${users.stackOverflow}`}>
                          <MDBIcon fab icon="stack-overflow" className="ml-2" />
                        </a>
                    )}
                  </MDBCol>
                </MDBRow>
                <MDBRow className="m-0">
                  <p className="cardText">Office: {users.office}</p>
                </MDBRow>
              </MDBContainer>
            </MDBCardBody>
          </MDBCard>
        </MDBAnimation>
      </MDBCol>
        ) : (
            <MDBCol size="6" md="2" className="p-0 m-0">
              <MDBAnimation type="zoomIn" duration="1s" className="d-flex justify-content-center">
                <MDBCard className="mt-3" style={{ width: "90%" }}>
                    <img
                        alt={`leetView of ${users.name}`}
                        className="img-fluid d-inline"
                        src={users.imagePortraitUrl}
                        />
                </MDBCard>
              </MDBAnimation>
            </MDBCol>
           )
        }
        </>
  )
}

export default ProfileCard;