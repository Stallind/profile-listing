import React from 'react';

const LeetView = ({ isLeetView, setIsLeetView }) => {

  return (
      <div className="custom-control custom-switch mt-4">
        <input
        type="checkbox"
        className="custom-control-input"
        id="customSwitches"
        readOnly
        checked={isLeetView}
        onChange={() => setIsLeetView(!isLeetView)}
        />
        <label className="custom-control-label" htmlFor='customSwitches'>
          Image only view
        </label>
      </div>
  )
}

export default LeetView;