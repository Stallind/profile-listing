import React from 'react';
import {MDBRow} from 'mdbreact';
import './SortButton.css'

const SortButtons = ({ sortByName, sortByOffice }) => {

  return (
      <div className="mt-3">
        <MDBRow className="d-flex justify-content-center font-weight-bold">
          Sort by
        </MDBRow>
        <button className='MDBBtn' onClick={() => sortByName()}>Name</button>
        <button className='MDBBtn ml-3' onClick={() => sortByOffice()}>Office</button>
      </div>
  )
}
export default SortButtons;
