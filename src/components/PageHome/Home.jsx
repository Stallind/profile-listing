import React, { useEffect, useState } from 'react'
import ProfileCard from '../PageProfileCard/ProfileCard';
import Header from '../PageHeader/Header';
import { MDBCol, MDBContainer, MDBRow } from 'mdbreact';
import SearchField from '../PageSearchField/SearchField';
import SortButtons from '../PageSortButtons/SortButtons';
import LeetView from '../PageLeetView/LeetView';

const Home = () => {

  const [users, setUsers] = useState([])
  const [displayedUsers, setDisplayedUsers] = useState([])
  const [isLeetView, setIsLeetView] = useState(false);


  useEffect(() => {
    fetch('https://api.tretton37.com/ninjas')
        .then(response => response.json())
        .then((data) => {
          setUsers(data);
        }).catch()
  },[])

  const sortByName = () => {
    const currentUserArray = [...displayedUsers]
    const sortedByName = currentUserArray.sort((a,b) => a.name.localeCompare(b.name))
    setDisplayedUsers(sortedByName);
  }

  const sortByOffice = () => {
    const currentUserArray = [...displayedUsers]
    const sortedByOffice = currentUserArray.sort((a, b) => a.office.localeCompare(b.office))
    setDisplayedUsers(sortedByOffice)
  }

  return(
      <MDBContainer className="mw-100">
        <MDBRow>
          <MDBCol className="d-flex justify-content-center mt-5">
            <Header />
          </MDBCol>
        </MDBRow>

        <MDBRow className="d-flex justify-content-center mb-2">
          <SearchField users={users} setDisplayedUsers={setDisplayedUsers} />
        </MDBRow>

        <MDBRow className="d-flex justify-content-center">
          <SortButtons sortByName={sortByName} sortByOffice={sortByOffice} />
        </MDBRow>

        <MDBRow className="d-flex justify-content-center">
          <LeetView isLeetView={isLeetView} setIsLeetView={setIsLeetView} />
        </MDBRow>

        <MDBContainer className="w-75 mt-2">
          <MDBRow>
            {displayedUsers.map((user, i) =>
                <ProfileCard
                    key={i}
                    users={user}
                    isLeetView={isLeetView}
                />)}
          </MDBRow>
        </MDBContainer>

      </MDBContainer>
  )
}

export default Home;