import React from 'react'
import './Header.css'

const Header = () => {

  return (
      <div className="d-flex">
        <h1 className="flash font-weight-bold">_</h1>
        <h1 className="font-weight-bold">meet our ninjas</h1>
      </div>
  )
}

export default Header;